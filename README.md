# ZetaBrain

#### Introduction

This is a simple neural network with RESTful API enabled.

```javascript
/*
  Control the Neural Network through API.

  1. train  [POST]

  e.g.

  config:{"inputSize":2,"outputSize":1,"expand":200,"name":"AND OPERATION"}
  options:{"iteration":100,"learningRate":0.02,"verbose":true,"incremental":true}
  trainDatas:[{"input":[0,0],"answer":0},{"input":[0,1],"answer":0},{"input":[1,0],"answer":0},{"input":[1,1],"answer":1}]
  testDatas:[{"input":[0,0],"answer":0},{"input":[0,1],"answer":0},{"input":[1,0],"answer":0},{"input":[1,1],"answer":1}]

  2. query  [GET]

  e.g.

  url[:port]/query?inputs=1

            OR

  url[:port]/query?inputs=[1,0]
*/
```

#### HOW TO USE

node index.js AND_OPT 3000

