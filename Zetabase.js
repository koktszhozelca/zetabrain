const fs = require("fs");

class Zetabase {
  static checkProject(){
    if(!fs.existsSync("./save"))
      fs.mkdir("./save", (err)=>{if(err) console.log(err)});
  }

  static wipe(){
    this._wipe("./save/config.json");
    this._wipe("./save/dataSet.json");
    this._wipe("./save/model.json");
  }

  static _wipe(filename){
    if(fs.existsSync(filename))
      fs.unlinkSync(filename);
  }

  static saveConfig(inputSize, outputSize, expand, name){
    var content = {inputSize, outputSize, expand, name};
    fs.writeFileSync("./save/config.json", JSON.stringify(content), (error)=>{
      if(error) console.log(error);
    })
  }

  static restoreConfig(){
    if(!fs.existsSync("./save/config.json")) return null;
    return new Promise((resolve, reject)=>{
      fs.readFile("./save/config.json", (err, json)=>{
        if(err) throw err;
        var content = JSON.parse(json);
        resolve(content);
      })
    });
  }

  static saveModel(layers, learningRate){
    var content = {
      layers: layers,
      learningRate: learningRate
    }
    fs.writeFileSync("./save/model.json", JSON.stringify(content), (error)=>{
      if(error) console.log(error);
    })
  }

  static restoreModel(){
    if(!fs.existsSync("./save/model.json")) return null;
    return new Promise((resolve, reject)=>{
      fs.readFile("./save/model.json", (err, json)=>{
        if(err) throw err;
        var content = JSON.parse(json);
        resolve(content);
      })
    });
  }

  static saveDataSet(dataSet){
    fs.writeFileSync("./save/dataSet.json", JSON.stringify(dataSet), (error)=>{
      if(error) console.log(error);
    })
  }

  static restoreDataSet(){
    if(!fs.existsSync("./save/dataSet.json")) return null;
    return new Promise((resolve, reject)=>{
      fs.readFile("./save/dataSet.json", (err, json)=>{
        if(err) throw err;
        var dataSet = JSON.parse(json);
        resolve(dataSet);
      })
    });
  }
}
module.exports = Zetabase;
