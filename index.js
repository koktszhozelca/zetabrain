const NeuralNetwork = require("./NeuralNetwork");
const Zetabase = require("./Zetabase");
const Endpoint = require("./Endpoint");

/*
  Control the Neural Network through API.

  1. train  [POST]

  e.g.

  config:{"inputSize":2,"outputSize":1,"expand":200,"name":"AND OPERATION"}
  options:{"iteration":100,"learningRate":0.02,"verbose":true,"incremental":true}
  trainDatas:[{"input":[0,0],"answer":0},{"input":[0,1],"answer":0},{"input":[1,0],"answer":0},{"input":[1,1],"answer":1}]
  testDatas:[{"input":[0,0],"answer":0},{"input":[0,1],"answer":0},{"input":[1,0],"answer":0},{"input":[1,1],"answer":1}]

  2. query  [GET]

  e.g.

  url[:port]/query?inputs=1

            OR

  url[:port]/query?inputs=[1,0]
*/

async function Main(name, port, restore=true){
  console.clear();
  Zetabase.checkProject();
  if(!restore) Zetabase.wipe();
  var nn = await NeuralNetwork.create(name);
  new Endpoint(nn, port);
}
// Main("AND OPERATION", 3000);
var name = process.argv[2] ? process.argv[2] : null;
var port = process.argv[3] ? process.argv[3] : null;
var restore = process.argv[4] ? process.argv[4] !== "false" : null;
Main(name, port, restore);
