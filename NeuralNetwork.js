const Matrix = require("zetamatrix");
const Calc = require("./Calc");
const Zetabase = require("./Zetabase");

var LAST_ERROR = Number.MAX_VALUE;
var LEARNING_RATE = 0.01;
var SATISFY_COUNT = 0;

class NeuralNetwork {
  constructor(name="Untitled NN") {
    this.name = name;
    this.layers = new Object();
    this.trainDataSet = null;
    this.inputSize = -1;
    this.outputSize = -1;
    this.expand = 100;
  }

  static async create(name="Untitled NN"){
    var nn = new NeuralNetwork(name);
    var model = await Zetabase.restoreModel();
    var config = await Zetabase.restoreConfig();
    var dataSet = await Zetabase.restoreDataSet();
    if(config)
      nn.config(config.inputSize, config.outputSize, config.expand, config.name, false);
    if(dataSet)
      nn.setTrainDataSet(dataSet);
    if(!model) return nn;
    LEARNING_RATE = model.learningRate;
    Object.keys(model.layers).map((layer)=>
      nn.layers[layer] = Matrix.restore(model.layers[layer]))
    return nn;
  }

  reset(){
    console.log("RESET");
    Zetabase.wipe();
    LEARNING_RATE = 0.01;
    LAST_ERROR = Number.MAX_VALUE;
    this.buildLayers();
  }

  config(inputSize, outputSize, expand, name="Untitled NN", reset = true){
    this.name = name;
    this.inputSize = inputSize;
    this.outputSize = outputSize;
    this.expand = expand;
    if(reset) this.reset();
    Zetabase.saveConfig(inputSize, outputSize, expand, name);
    // console.log("Configuration is updated.");
  }

  setTrainDataSet(trainDataSet){
    this.trainDataSet = trainDataSet;
    this.dataSet = this.expandDataSet(trainDataSet, this.expand);
    Zetabase.saveDataSet(this.trainDataSet);
    // console.log("DataSet is updated, entry count: ", this.dataSet.length);
  }

  addTrainData(trainDatas){
    this.dataSet = null;
    if(Array.isArray(trainDatas))
      for(var i in trainDatas)
        this.trainDataSet.push(trainDatas[i]);
    else this.trainDataSet.push(trainDatas);
    this.dataSet = this.expandDataSet(this.trainDataSet, this.expand);
    Zetabase.saveDataSet(this.trainDataSet);
    // console.log("DataSet is updated, entry count: ", this.dataSet.length);
  }

  buildLayers(){
    this.hiddenLayer();
    this.outputLayer();
  }

  classify(dataSet){
    var outputs = [];
    for(var i=0; i<dataSet.length; i++)
      outputs.push(Object.values(this._classify(dataSet[i]).data));
    return outputs
  }

  _classify(data){
    var intermediate = this.feedForward(data);
    return intermediate.outputMatrix;
  }

  train(learningRate=0.01, iteration=1, verbose=false){
    LEARNING_RATE = learningRate;
    var total = iteration;
    while(iteration-- > 0) {
      console.clear();
      if(!verbose) console.log("Training, please wait...",
                      Math.round((total-iteration) / total * 100) + "%");
      if(!this._train(verbose)) break;
    }
    Zetabase.saveModel(this.layers, LEARNING_RATE);
    return Promise.resolve()
  }

  _train(verbose){
    for(var i=0; i<this.dataSet.length; i++){
      var data = this.dataSet[i];
      var intermediate = this.feedForward(data);
      if(!this.backPropagation(intermediate, verbose)) return false;
    }
    return true;
  }

  outputLayer(length){
    this.layers["Output"] = new Matrix(this.outputSize, 1).passFunc(Calc.randomWeight);
    return this;
  }

  hiddenLayer(){
    var length = Math.ceil(this.inputSize * 2/3 + this.outputSize);
    this.layers["Hidden"] = new Matrix(length, 1).passFunc(Calc.randomWeight);
    this.edges("I-H", length, this.inputSize);
    this.edges("H-O", this.outputSize, length);
    return this;
  }

  edges(name, row, col){
    this.layers[name] = new Matrix(row, col).passFunc(Calc.randomWeight);
    return this;
  }

  feedForward(data){
    var {matrix, answer} = this.trainConfig(data);
    var ihEdges = this.layers["I-H"].clone();
    var hMatrix = this.layers["Hidden"].clone();
    var hoEdges = this.layers["H-O"].clone();
    var oMatrix = this.layers["Output"].clone();
    var intermediate = new Object();
    intermediate.inputMatrix = matrix.clone();
    matrix = ihEdges.multiply(matrix)
                    .add(hMatrix)
                    .passFunc(Calc.sigmoid);
    intermediate.hiddenMatrix = matrix.clone();
    matrix = hoEdges.multiply(matrix)
                    .add(oMatrix)
                    .passFunc(Calc.sigmoid);
    intermediate.outputMatrix = matrix.clone();
    intermediate.answer = answer ? answer.clone() : null;
    return intermediate;
  }

  backPropagation(intermediate, verbose){
    var outputs = intermediate.outputMatrix;
    var input = intermediate.inputMatrix;
    var hidden = intermediate.hiddenMatrix;
    var answer = intermediate.answer;
    //Output errors
    var outputErrs = answer.subtract(outputs);
    var hoGradients = outputs.passFunc(Calc.dSigmoid)
                             .entrywiseProduct(outputErrs)
                             .multiply(LEARNING_RATE);
    var hoDeltaWeight = hoGradients.clone().multiply(hidden.transpose());
    this.layers["H-O"].add(hoDeltaWeight);
    this.layers["Output"].add(hoGradients);
    //Hidden errors
    var hoEdges = this.layers["H-O"].clone();
    var hiddenErrs = hoEdges.transpose().multiply(outputErrs)
    var ihGradients = hidden.passFunc(Calc.dSigmoid)
                            .entrywiseProduct(hiddenErrs)
                            .multiply(LEARNING_RATE)
    var ihDeltaWeight = ihGradients.clone().multiply(input.transpose());
    this.layers["I-H"].add(ihDeltaWeight);
    this.layers["Hidden"].add(ihGradients);
    return this.evaluation(outputErrs, verbose);
  }

  evaluation(outputErrs, verbose){
    var curErr = 0;
    outputErrs.readAll((data, row, col)=>curErr += Math.abs(data))
    if(verbose) console.log("Cur Error: " + curErr +
                            "".padEnd(5) + "Learning rate: " + LEARNING_RATE);
    // LEARNING_RATE = LAST_ERROR > curErr ?
    //                   LEARNING_RATE + curErr / LAST_ERROR:
    //                   LEARNING_RATE;
    // LAST_ERROR = LAST_ERROR > curErr ? curErr : LAST_ERROR;

    // SATISFY_COUNT = LAST_ERROR < 0.0001 ? SATISFY_COUNT + 1 : 0;
    // if(SATISFY_COUNT === 100) console.log("REACH TARGET EXIT: ", LAST_ERROR);
    // return SATISFY_COUNT !== 100;
    return true;
  }

  trainConfig(data){
    return {
      "matrix": this._trainConfig(data.input),
      "answer": data.hasOwnProperty("answer") ? this._trainConfig(data.answer) : null
    }
  }

  _trainConfig(data){
    if((typeof data).toString().toLowerCase() === "number")
      return new Matrix(1, 1).updateAll(data);
    var m = new Matrix(data.length, 1);
    for(var i=0; i<data.length; i++)
      m.update(i, 0, data[i]);
    return m;
  }

  expandDataSet(dataSet, expandTo){
    var result = [];
    for(var i=0; i<dataSet.length * expandTo; i++)
      result.push(this.pickDataEntry(dataSet))
    return result;
  }

  pickDataEntry(dataSet){
    var size = dataSet.length;
    var pick = Math.floor(Math.random() * size);
    return dataSet[pick];
  }

  print(){
    Object.keys(this.layers).map((layer)=>{
      console.log(layer);
      this.layers[layer].print();
      console.log("\n");
    })
  }
}
module.exports = NeuralNetwork;
