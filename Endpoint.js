const ip = require('ip').address();
const express = require("express");
const bodyParser = require('body-parser');
const NeuralNetwork = require("./NeuralNetwork");

class Endpoint {
  constructor(neuralNetwork, port=3000) {
    this.nn = neuralNetwork;
    this.port = port;
    this.endpoint = express();
    this.start();
  }

  protocols(){
    return {
      POST : {
        train: (req, res)=>{
          var config = JSON.parse(req.body.config);
          var trainDatas = JSON.parse(req.body.trainDatas);
          var options = JSON.parse(req.body.options);
          var testDatas = JSON.parse(req.body.testDatas);
          if(!options.incremental) {
            this.nn.config (
              config.inputSize, config.outputSize,
              config.expand, config.name
            );
            this.nn.setTrainDataSet(trainDatas);
          }
          this.nn.train(options.learningRate, options.iteration, options.verbose);
          var testResult = this.nn.classify(testDatas);
          var response = {
            status:200,
            message: "Training is completed.",
            test: testResult
          };
          console.clear();
          console.log("[ "+this.nn.name + " ]", "Endpoint service is online on port: ", this.port);
          console.log("[ SYSTEM MSG ]\n" + JSON.stringify(response, null, ' '));
          res.json(response);
        },
        demo: (req, res)=>{
          var trainDatas = [
            {input:[0,0], answer:[0,0]},
            {input:[0,1], answer:[0,1]},
            {input:[1,0], answer:[1,0]},
            {input:[1,1], answer:[1,1]}
          ]
          this.nn.config(2, 2, 100, "DEMO");
          this.nn.setTrainDataSet(trainDatas);
          this.nn.train(100, false);
          var testResult = this.nn.classify(trainDatas);
          var response = {
            status:200,
            message: "Training is completed.",
            test: testResult
          }
          console.clear();
          console.log("[ "+this.nn.name + " ]", "Endpoint service is online on port: ", this.port);
          console.log("[ SYSTEM MSG ]\n" + JSON.stringify(response, null, ' '));
          res.json(response);
        },
        exit: (req, res)=>{
          console.clear();
          console.log("[ "+this.nn.name + " ]", "Endpoint service is online on port: ", this.port);
          console.log("[ SYSTEM MSG ]\n" + "Exit");
          process.exit(0);
        }
      },
      GET : {
        query: (req, res)=>{
          var inputs, result, response;
          try {
            inputs = JSON.parse(req.query.inputs);
            result = Object.values(this.nn._classify({input:inputs}).data);
            response = {
              status: 200,
              message: result
            }
            console.log("[ SYSTEM MSG ]\n" + JSON.stringify(response, null, ' '));
            res.json(response);
          } catch(e){
            console.log(e);
            response = {
              status: 500,
              message: "Server error"
            }
            console.log("[ SYSTEM MSG ]\n" + JSON.stringify(response, null, ' '));
            res.json(response);
          }
        }
      }
    }
  }

  start(){
    this.endpoint.use(bodyParser.urlencoded({ extended: false }))
    this.endpoint.use(bodyParser.json())
    var protocols = this.protocols();

    Object.keys(protocols.POST).map((action)=>{
      this.endpoint.post("/"+action, protocols.POST[action]);
    })

    Object.keys(protocols.GET).map((action)=>{
      this.endpoint.get("/"+action, protocols.GET[action]);
    })

    this.endpoint.listen(this.port, ()=>{
      console.log("[ "+this.nn.name + " ]", "Endpoint service is online on ", ip+":"+this.port);
    })
  }
}
module.exports = Endpoint;
